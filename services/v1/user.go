package v1s

import (
	api "blog/apiHelper"
	helper "blog/helper"
	model "blog/model"
	v1r "blog/resources/v1"
	"net/http"

	log "github.com/sirupsen/logrus"

	"time"

	"github.com/gin-gonic/gin"
)

// CreateUser 创建用户
func CreateUser(c *gin.Context, u *v1r.UserCreateRequest) (code int) {
	// 1. 验证数据合法性
	if valid, err := helper.Valid(u); !valid {
		log.Error("err user create === ", err)
		return api.INVALID_REQUEST_PARAMS
	}

	// 组装model数据
	user := model.User{
		Email:     u.Email,
		Username:  u.Username,
		Password:  u.Password,
		Passcode:  helper.RandPcode(6),
		LoginIP:   c.ClientIP(),
		LoginTime: time.Now(),
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	// 是否存在用户名
	if existedUname, _ := model.FindUname(user.Username); existedUname {
		return api.USER_NAME_EXISTED
	}

	// 邮箱是否已经注册过
	if existedEmail, _ := model.FindEmail(user.Email); existedEmail {
		return api.USER_EMAIL_EXISTED
	}

	// 加密密码
	cryptPwd, err := helper.EncryptPwd(user.Password + user.Passcode)
	if err != nil {
		log.Error("加密出错 === ", err)
		return api.INTERNAL_SERVER_ERROR
	}
	user.Password = cryptPwd

	// 创建用户
	if createdOK, _ := user.Create(); createdOK {
		created, role := model.CreateRole(user.Username)
		if created {
			log.Info("role", role)
		}

		return api.USER_CREATE_SUCCESS
	}

	return api.USER_CREATE_ERROR
}

// Login 用户登录接口
func Login(c *gin.Context, u *v1r.UserLoginRequest) (code int, data *v1r.UserLoginResponse) {
	if valid, err := helper.Valid(u); !valid {
		log.Error("err user create === ", err)
		return api.USER_LOGIN_INVALIDPWD, data
	}

	if valid := u.UserLoginValidate(); !valid {
		return api.USER_LOGIN_NILEMUN, data
	}

	findOut, user := model.FindUserByNameOrEmail(u)
	if !findOut {
		return api.USER_NOT_FOUND, data
	}
	str := u.Password + user.Passcode
	if err := helper.ComparePwd(user.Password, str); err != nil {
		log.Info("error", err)
		return api.USER_INVALID_PWD, data
	}

	roleName := model.FindUserRoleByUsername(user.Username)

	data = &v1r.UserLoginResponse{
		Username:  user.Username,
		UpdatedAt: user.UpdatedAt.Format("2006-01-02 15:04:05"),
		Role:      roleName,
	}

	// 签发token到cookie
	tc, code := helper.Sign(c, &helper.TokenContext{
		ID:       1,
		Username: user.Username,
	}, "")

	t := time.Now().Second() + 60*60*1000
	cookie := &http.Cookie{
		Name:     "token",
		Value:    tc,
		HttpOnly: true,
		Path:     "/",
		Secure:   false,
		Domain:   helper.GetDomain(),
	}

	// 设置cookie
	c.SetCookie(cookie.Name, cookie.Value, t, cookie.Path, cookie.Domain, cookie.Secure, cookie.HttpOnly)

	// 用户名 密码  或者 邮箱 密码  或者cookie
	return api.USER_LOGIN_SUCCESS, data
}

// UserList 获取用户列表
func UserList(c *gin.Context, ul *v1r.UserListRequest) (code int, users *v1r.UserListResponse) {
	if valid, err := helper.Valid(ul); !valid {
		log.Error("err user list === ", err)
		return api.INVALID_REQUEST_PARAMS, users
	}

	result, total, err := model.ListUser(ul)
	if err != nil {
		return api.SQL_HANDLE_ERR, users
	}

	var lists []*v1r.UserResponse
	for _, user := range result {
		u := &v1r.UserResponse{
			Username:  user.Username,
			CreatedAt: user.CreatedAt.Format("2006-01-02 15:04:05"),
		}
		lists = append(lists, u)
	}

	users = &v1r.UserListResponse{
		Total: total,
		List:  lists,
	}

	return api.SUCCESS, users
}

// UserRole 获取用户权限
func UserRole(ctx *helper.TokenContext) (int, *v1r.UserRoleResponse) {
	if ctx == nil {
		return api.USER_NOT_LOGIN, &v1r.UserRoleResponse{}
	}

	uname := ctx.Username

	roleName := model.FindUserRoleByUsername(uname)

	// 获取用户的权限
	return api.USER_HAD_PRIVILEGE, &v1r.UserRoleResponse{
		Username:  ctx.Username,
		Privilege: roleName,
	}
}
