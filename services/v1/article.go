package v1s

import (
	api "blog/apiHelper"
	helper "blog/helper"
	model "blog/model"
	v1r "blog/resources/v1"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

// CreateArticle 发布新文章
func CreateArticle(c *gin.Context, ac *v1r.ArticleCreateRequest) int {
	if valid, err := helper.Valid(ac); !valid {
		log.Error("Error occured when validate article request params: ", err)
		return api.INVALID_REQUEST_PARAMS
	}

	// 1. author 没有，解析cookie, cookie没有 '-'
	author, code := helper.ParseRequest(c)
	if code != api.SUCCESS {
		return api.USER_NOT_LOGIN
	}
	// 2. AbbrContent 没有 取Content前80个字
	if ac.AbbrContent == "" {
		lenContent := len(ac.Content)
		var len int = lenContent
		if lenContent > 80 {
			len = 80
		}
		ac.AbbrContent = string([]rune(ac.Content)[:len])
	}

	article := model.Article{
		Title:       ac.Title,
		Author:      author.Username,
		Content:     ac.Content,
		AbbrContent: ac.AbbrContent,
		Markdown:    ac.Markdown,
		PubDate:     time.Now(),
		CreatedAt:   time.Now(),
		Tags:        ac.Tags,
		Views:       0,
		Comments:    0,
		Likes:       0,
		Tops:        0,
	}
	log.Info("article", article)
	if createOK := article.Create(); createOK {
		return api.ARTICLE_CREATE_SUCCESS
	}

	return api.ARTICLE_CREATE_ERROR
}

// ArtilesList 获取文章列表
func ArtilesList(c *gin.Context, al *v1r.ArticleListRequest) (code int, ats *v1r.ArticleListResponse) {
	if valid, err := helper.Valid(al); !valid {
		log.Error("Error occured when validate articleList request params: ", err)
		return api.INVALID_REQUEST_PARAMS, ats
	}

	result, total, err := model.ListArticle(al)
	if err != nil {
		return api.SQL_HANDLE_ERR, ats
	}

	var lists []*v1r.ArticleResponse
	for _, article := range result {
		ar := &v1r.ArticleResponse{
			ID:          article.ID,
			Title:       article.Title,
			AbbrContent: article.AbbrContent,
			Views:       article.Views,
			Likes:       article.Likes,
			Content:     article.Content,
			Comments:    article.Comments,
			CreatedAt:   article.CreatedAt.Format("2006-01-02 15:04:05"),
		}
		lists = append(lists, ar)
	}

	articles := &v1r.ArticleListResponse{
		Total: total,
		Lists: lists,
	}

	return api.SUCCESS, articles
}

// ArticleDetail 获取文章详情
func ArticleDetail(c *gin.Context, adid string) (code int, ads *v1r.ArticleResponse) {
	findOut, result := model.DetailArticle(adid)
	if !findOut {
		return api.SQL_HANDLE_ERR, ads
	}

	ads = &v1r.ArticleResponse{
		ID:          result.ID,
		Title:       result.Title,
		AbbrContent: result.AbbrContent,
		CreatedAt:   result.CreatedAt.Format("2006-01-02 15:04:05"),
		Views:       result.Views,
		Likes:       result.Likes,
		Content:     result.Content,
		Comments:    result.Comments,
	}

	return api.SUCCESS, ads
}
