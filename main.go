package main

import (
	"blog/config"
	"blog/model"
	"blog/router"
	v "blog/seeder/version"
	"encoding/json"

	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
)

// 初始化配置文件名
var (
	cfg     = ""
	version = pflag.BoolP("version", "v", false, "show version info.")
)

//Execution starts from main function
func main() {
	pflag.Parse()

	if *version {
		v := v.Get()
		marshalled, err := json.MarshalIndent(&v, "", "  ")
		if err != nil {
			log.Infof("%v\n", err)
			os.Exit(1)
		}

		log.Info(string(marshalled))
		return
	}

	if err := config.Init(cfg); err != nil {
		panic(err)
	}

	model.DB.Init()

	defer model.DB.Close()

	mode := viper.Get("mode")
	var currentMode string
	if mode == nil {
		currentMode = "debug"
	} else {
		currentMode = mode.(string)
	}
	gin.SetMode(currentMode)
	g := gin.New()
	middleware := []gin.HandlerFunc{}
	router.SetupRouter(
		g,
		middleware...,
	)

	log.Info("Start to listening the requests on http address ", viper.GetString("addr"))
	http.ListenAndServe(viper.GetString("addr"), g)
}
