package helper

import (
	api "blog/apiHelper"
	// "fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// TokenContext token struct
type TokenContext struct {
	ID       uint64
	Username string
}

// secretFunc 验证密码
func secretFunc(secret string) jwt.Keyfunc {
	return func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, jwt.ErrSignatureInvalid
		}
		return []byte(secret), nil
	}
}

// Parse 解析token
func Parse(tokenString string, secret string) (*TokenContext, error) {
	ctx := &TokenContext{}

	token, err := jwt.Parse(tokenString, secretFunc(secret))
	if err != nil {
		return ctx, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		ctx.ID = uint64(claims["id"].(float64))
		ctx.Username = claims["username"].(string)
		return ctx, nil
	}

	return ctx, err
}

// ParseRequest 解析请求
func ParseRequest(c *gin.Context) (*TokenContext, int) {
	cookie, err := c.Cookie("token")
	if err != nil {
		log.Info("err ====", err)
		return &TokenContext{}, api.PARSE_TOKEN_FAILURE
	}
	log.Info("cookie ====", cookie)
	secret := viper.GetString("jwt_secret")
	if len(cookie) == 0 {
		return &TokenContext{}, api.USER_NOT_LOGIN
	}

	ctx, err := Parse(cookie, secret)
	if err != nil {
		return ctx, api.PARSE_TOKEN_FAILURE
	}

	return ctx, api.SUCCESS
}

// Sign 签署token
func Sign(c *gin.Context, tc *TokenContext, secret string) (string, int) {
	if secret == "" {
		secret = viper.GetString("jwt_secret")
	}

	// The token content.
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":       tc.ID,
		"username": tc.Username,
		"nbf":      time.Now().Unix(),
		"iat":      time.Now().Unix(),
	})
	// Sign the token with the specified secret.
	tokenString, err := token.SignedString([]byte(secret))

	if err != nil {
		return tokenString, api.PARSE_TOKEN_FAILURE
	}

	return tokenString, api.SUCCESS
}
