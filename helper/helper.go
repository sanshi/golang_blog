package helper

import (
	"crypto/rand"
	"fmt"
	random "math/rand"
	"reflect"
	"strconv"

	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"

	"github.com/gin-gonic/gin"
	validator "gopkg.in/go-playground/validator.v9"

	log "github.com/sirupsen/logrus"
)

// Int64ToString 变int 为 string
func Int64ToString(inputNum int64) string {
	// to convert a float number to a string
	return strconv.FormatInt(inputNum, 10)
}

// CreateUUID 生成一个唯一的UUID
func CreateUUID() (uuid string) {
	u := new([16]byte)
	_, err := rand.Read(u[:])
	if err != nil {
		log.Error("Cannot generate UUID", err)
	}

	// 0x40 is reserved variant from RFC 4122
	u[8] = (u[8] | 0x40) & 0x7F
	// Set the four most significant bits (bits 12 through 15) of the
	// time_hi_and_version field to the 4-bit version number.
	u[6] = (u[6] & 0xF) | (0x4 << 4)
	uuid = fmt.Sprintf("%x-%x-%x-%x-%x", u[0:4], u[4:6], u[6:8], u[8:10], u[10:])
	return
}

// GetContextIP 获取ip
func GetContextIP(c *gin.Context) (IP string) {
	return c.Request.RemoteAddr
}

// JSONFieldError 过滤field是哪个
type JSONFieldError struct {
	NameSpace       string
	Field           string
	StructNamespace string
	StructField     string
	Tag             string
	Kind            string
	ActualTag       string
	Value           string
	Param           string
	Type            reflect.Type
}

// FormatValidateField 格式化校验错误的字段
func FormatValidateField(err error) JSONFieldError {
	var fieldErr JSONFieldError

	if _, ok := err.(*validator.InvalidValidationError); ok {
		fmt.Println("====", err)
		return fieldErr
	}

	for _, err := range err.(validator.ValidationErrors) {
		fieldErr = JSONFieldError{
			Field:     err.Field(),
			Tag:       err.Tag(),
			ActualTag: err.ActualTag(),
			Type:      err.Type(),
		}
	}

	return fieldErr
}

var validate *validator.Validate

// Valid 验证结构体是否符合
func Valid(s interface{}) (bool, error) {
	validate = validator.New()
	if err := validate.Struct(s); err != nil {
		return false, err
	}
	return true, nil
}

// RandPcode 生成随机6位随机数
func RandPcode(l int) string {
	str := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	result := []byte{}
	for i := 0; i < l; i++ {
		r := random.Intn(62)
		result = append(result, str[r])
	}
	return string(result)
}

// EncryptPwd 加密字符串
func EncryptPwd(source string) (string, error) {
	hashedBytes, err := bcrypt.GenerateFromPassword([]byte(source), bcrypt.MinCost)
	return string(hashedBytes), err
}

// ComparePwd 比较密码
func ComparePwd(hashedPwd, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(password))
}

// GetDomain 设域名
func GetDomain() string {
	mode := viper.Get("mode")

	if mode == nil {
		mode = "debug"
	}

	if mode == "debug" {
		return "localhost"
	}
	log.Info("domain mode ==", mode)
	return "alisuns.com"
}
