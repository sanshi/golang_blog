package v1ctrl

import (
	api "blog/apiHelper"
	v1r "blog/resources/v1"
	v1s "blog/services/v1"

	"github.com/gin-gonic/gin"
)

// ArticleCreate 创建一篇新文章
func ArticleCreate(c *gin.Context) {
	var ac v1r.ArticleCreateRequest
	if err := c.ShouldBind(&ac); err != nil {
		api.Response(c, api.INVALID_REQUEST_PARAMS, nil)
		return
	}

	code := v1s.CreateArticle(c, &ac)
	api.Response(c, code, nil)
}

// ArticleList 获取文章列表
func ArticleList(c *gin.Context) {
	var al v1r.ArticleListRequest
	if err := c.ShouldBind(&al); err != nil {
		api.Response(c, api.INVALID_REQUEST_PARAMS, nil)
		return
	}

	code, data := v1s.ArtilesList(c, &al)
	api.Response(c, code, data)
}

// ArticleDetail 获取文章详情
func ArticleDetail(c *gin.Context) {
	var adid = c.Param("id")

	code, data := v1s.ArticleDetail(c, adid)
	api.Response(c, code, data)
}
