package v1ctrl

import (
	api "blog/apiHelper"
	helper "blog/helper"
	v1r "blog/resources/v1"
	"net/http"
	"time"

	v1s "blog/services/v1"

	"github.com/gin-gonic/gin"
	// log "github.com/sirupsen/logrus"
)

// UserCreate 创建用户
func UserCreate(c *gin.Context) {
	// 检查用户名，email是否已经存在
	// 0. 解析参数
	var u v1r.UserCreateRequest
	if err := c.ShouldBind(&u); err != nil {
		api.Response(c, api.INVALID_REQUEST_PARAMS, nil)
		return
	}

	code := v1s.CreateUser(c, &u)
	api.Response(c, code, nil)
}

// UserLogin 用户登录接口
func UserLogin(c *gin.Context) {
	var u v1r.UserLoginRequest
	if err := c.ShouldBind(&u); err != nil {
		api.Response(c, api.INVALID_REQUEST_PARAMS, nil)
		return
	}

	code, data := v1s.Login(c, &u)
	api.Response(c, code, data)
}

// UserList 获取用户列表
func UserList(c *gin.Context) {
	var ul v1r.UserListRequest
	if err := c.ShouldBind(&ul); err != nil {
		api.Response(c, api.INVALID_REQUEST_PARAMS, nil)
		return
	}

	code, data := v1s.UserList(c, &ul)
	api.Response(c, code, data)
}

// UserLogout 退出登录
func UserLogout(c *gin.Context) {
	t := time.Now().Second()
	cookie := &http.Cookie{
		Name:     "token",
		Value:    "",
		HttpOnly: true,
		Path:     "/",
		Secure:   false,
		Domain:   helper.GetDomain(),
	}

	// 设置cookie
	c.SetCookie(cookie.Name, cookie.Value, t, cookie.Path, cookie.Domain, cookie.Secure, cookie.HttpOnly)
	api.Response(c, api.USER_LOGOUT_SUCCESS, nil)
}

// UserPrivilege 获取用户权限
func UserPrivilege(c *gin.Context) {
	ctx, code := helper.ParseRequest(c)
	if code != api.SUCCESS {
		api.Response(c, api.USER_NOT_LOGIN, nil)
		return
	}

	code, data := v1s.UserRole(ctx)
	api.Response(c, code, data)
}

// SetUserPrivilege 设置用户权限
func SetUserPrivilege(c *gin.Context) {

}
