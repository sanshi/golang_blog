
SHELL := /bin/zsh
BASEDIR = $(shell pwd)

# build with verison infos
versionDir = "blog/seeder/version"
gitTag = $(shell if [ "`git describe --tags --abbrev=0 2>/dev/null`" != "" ];then git describe --tags --abbrev=0; else git log --pretty=format:'%h' -n 1; fi)
buildDate = $(shell TZ=Asia/Shanghai date +%FT%T%z)
gitCommit = $(shell git log --pretty=format:'%H' -n 1)
gitTreeState = $(shell if git status|grep -q 'clean';then echo clean; else echo dirty; fi)

ldflags="-w -X ${versionDir}.gitTag=${gitTag} -X ${versionDir}.buildDate=${buildDate} -X ${versionDir}.gitCommit=${gitCommit} -X ${versionDir}.gitTreeState=${gitTreeState}"

all: gotool
	go build -v -ldflags ${ldflags}

clean:
	rm -f blog	&& rm -f ./release
	find . -name "[._]*.s[a-w][a-z]" | xargs rm -f {}

gotool:
	gofmt -w .
	go vet . | grep -v vendor; true

ca:
	# openssl req -new -nodes -x509 -out conf/server.crt -keyout conf/server.key -days 3650 -subj "/C=DE/ST=NRW/L=Earth/O=Random Company/OU=IT/CN=127.0.0.1/emailAddress=xxxxx@qq.com"

help:
	@echo "make - compile the source code"
	@echo "make clean - remove binary file and vim swp files"
	@echo "make gotool - run go tool 'fmt' and 'vet'"
	@echo "make ca - generate ca files"

swag:
	/Users/sanshi/go/bin/swag init
	# /Users/alisa/go/bin/swag init

run:
	go run main.go

build:  gotool
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -ldflags ${ldflags}

remote:
	make build
	mkdir ./release && mv blog release
	scp -r release root@123.206.18.142:/tmp
	rm -rf ./release

.PHONY: clean go tool ca help
