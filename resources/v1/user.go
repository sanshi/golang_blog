package v1r

// UserCreateRequest 注册用户请求数据结构
type UserCreateRequest struct {
	Username string `validate:"required" json:"username"`
	Password string `validate:"required" json:"password"`
	Email    string `validate:"required,email" json:"email"`
}

// UserLoginRequest 登录用户数据结构
type UserLoginRequest struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `validate:"required" json:"password"`
}

// UserLoginResponse 登录返回结果
type UserLoginResponse struct {
	Username  string `json:"username"`
	UpdatedAt string `json:"lastLoginTime"`
	Role      string `json:"role"`
}

// UserListRequest 获取用户列表
type UserListRequest struct {
	Limit int `form:"limit"`
	Page  int `validate:"min=1" form:"page"`
}

// UserListResponse 用户列表返回值
type UserListResponse struct {
	Total uint64          `json:"total"`
	List  []*UserResponse `json:"lists"`
}

// UserResponse 返回用户数据的数据结构
type UserResponse struct {
	Username  string `json:"username"`
	CreatedAt string `json:"createdAt"`
}

// UserRoleResponse 用户权限返回数据结构
type UserRoleResponse struct {
	Username  string `json:"username"`
	Privilege string `json:"role"`
}

// UserLoginValidator 验证login接口参数
func (ul *UserLoginRequest) UserLoginValidate() bool {
	if ul.Username == "" && ul.Email == "" {
		return false
	}

	return true
}
