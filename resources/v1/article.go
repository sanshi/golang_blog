package v1r

// ArticleResponse 获取文章通用的返回结构
type ArticleResponse struct {
	ID          uint64 `json:"id"`
	Title       string `json:"title"`
	AbbrContent string `json:"abbrContent"`
	CreatedAt   string `json:"createdAt"`
	Views       uint64 `json:"views"`
	Likes       uint64 `json:"likes"`
	Content     string `json:"content"`
	Comments    uint64 `json:"comments"`
}

// ArticleCreateRequest 创建文章请求数据结构
type ArticleCreateRequest struct {
	Title       string `json:"title"`
	Content     string `json:"content"`
	Tags        string `json:"tags"`
	Markdown    int    `json:"markdown"`
	AbbrContent string `json:"abbrContent"`
}

// ArticleCreateResponse 创建文章后的返回数据结构
type ArticleCreateResponse struct {
}

// ArticleListRequest 获取文章列表的请求数据结构
type ArticleListRequest struct {
	Limit int `form:"limit"`
	Page  int `form:"page"`
}

// ArticleListResponse 获取文章列表的返回结构
type ArticleListResponse struct {
	Total uint64             `json:"total"`
	Lists []*ArticleResponse `json:"lists"`
}

// ArticleDetailRequest 获取文章详情的请求结构
type ArticleDetailRequest struct {
	ID uint64 `uri:"id"`
}

// ArticleDetailResponse 获取文章详情返回结构
// 同 ArticleResponse
