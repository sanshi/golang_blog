package router

import (
	v1ctrl "blog/controller/v1"
	"blog/middleware"
	"net/http"

	"github.com/gin-gonic/gin"
)

// SetupRouter 设置路由
func SetupRouter(g *gin.Engine, mw ...gin.HandlerFunc) *gin.Engine {
	g.Use(gin.Logger())
	g.Use(gin.Recovery())
	g.Use(middleware.NoCache)
	g.Use(middleware.Options)
	g.Use(middleware.Secure)
	g.Use(middleware.RequestID())
	g.Use(mw...)

	// g.Static("/static", "static")
	// g.LoadHTMLGlob("templates/*")

	// 404
	g.NoRoute(func(c *gin.Context) {
		c.String(http.StatusNotFound, "404 The incorrect API route")
	})

	v1user := g.Group("/api/v1blog/user")
	{
		v1user.POST("/create", v1ctrl.UserCreate)
		v1user.POST("/login", v1ctrl.UserLogin)
		v1user.GET("/logout", v1ctrl.UserLogout)
		v1user.GET("/list", middleware.AuthMiddleware(), v1ctrl.UserList)
		v1user.GET("/privilege", v1ctrl.UserPrivilege)
	}

	v1Article := g.Group("/api/v1blog/article")
	{
		v1Article.POST("/create", middleware.AuthMiddleware(), v1ctrl.ArticleCreate)
		v1Article.GET("/list", v1ctrl.ArticleList)
		v1Article.GET("/detail/:id", v1ctrl.ArticleDetail)
	}

	return g
}
