package model

import (
	v1r "blog/resources/v1"
	"time"
)

// Article 文章
type Article struct {
	ID          uint64
	Title       string
	Author      string
	Content     string
	AbbrContent string
	Markdown    int
	PubDate     time.Time
	CreatedAt   time.Time
	Tags        string
	Views       uint64
	Comments    uint64
	Likes       uint64
	Tops        uint64
}

// Table 获取表的名字
func (a *Article) Table() string {
	return "articles"
}

// Create 往数据库添加文章
func (a *Article) Create() bool {
	if err := DB.Self.Create(&a).Error; err != nil {
		return false
	}

	return true
}

// ListArticle 获取文章的列表
func ListArticle(la *v1r.ArticleListRequest) (result []*Article, count uint64, err error) {
	limit := DEFAULTLIMIT
	page := DEFAULTPAGE
	if la.Limit != 0 {
		limit = la.Limit
	}

	if la.Page > 1 {
		page = la.Page - 1
	}
	offset := limit * page

	if err := DB.Self.Table("articles").Count(&count).Error; err != nil {
		return result, count, err
	}

	if err := DB.Self.Model(&Article{}).Offset(offset).Limit(limit).Order("id").Find(&result).Error; err != nil {
		return result, count, err
	}

	return result, count, nil
}

// DetailArticle 获取文章详情
func DetailArticle(adid string) (findOut bool, article *Article) {
	result := &Article{}

	err := DB.Self.Where("id = ?", adid).First(&result).Error
	if err != nil {
		return false, result
	}

	return true, result
}
