package model

import (
	"time"

	v1r "blog/resources/v1"

	log "github.com/sirupsen/logrus"
)

// User 结构体
type User struct {
	Passcode  string
	Username  string `gorm:"UNIQUE"`
	Email     string `gorm:"UNIQUE"`
	Password  string
	LoginIP   string
	LoginTime time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
}

// TableName 表名
func (u *User) TableName() string {
	return "user_info"
}

// FindUname 通过用户名查询用户
func FindUname(uname string) (bool, *User) {
	result := &User{}

	err := DB.Self.Where("username = ?", uname).First(&result).Error
	if err != nil {
		return false, result
	}

	return true, result
}

// FindEmail 通过邮箱查询用户
func FindEmail(email string) (bool, *User) {
	result := &User{}
	err := DB.Self.Where("email = ?", email).First(&result).Error

	if err != nil {
		return false, result
	}

	return true, result
}

// Create 创建用户
func (u *User) Create() (bool, *User) {
	if err := DB.Self.Create(&u).Error; err != nil {
		return false, u
	}

	return true, u
}

// ListUser 分页查询所有的用户
func ListUser(ul *v1r.UserListRequest) (result []*User, count uint64, err error) {
	limit := DEFAULTLIMIT
	page := DEFAULTPAGE
	if ul.Limit != 0 {
		limit = ul.Limit
	}

	if ul.Page > 1 {
		page = ul.Page - 1
	}

	if err := DB.Self.Table("user_info").Count(&count).Error; err != nil {
		return result, count, err
	}

	if err := DB.Self.Model(&User{}).Offset(page * limit).Limit(limit).Order("id").Find(&result).Error; err != nil {
		return result, count, err
	}
	return result, count, nil
}

// FindUserByNameOrEmail 通过用户名或者邮箱查找用户
func FindUserByNameOrEmail(u *v1r.UserLoginRequest) (bool, *User) {
	var user *User
	if findOut, user := FindUname(u.Username); findOut {
		return true, user
	}

	if findOut, user := FindEmail(u.Email); findOut {
		return true, user
	}

	return false, user
}

// FindUserRoleByUsername 通过用户名查找用户角色
func FindUserRoleByUsername(name string) string {
	result := &UserAuthority{}
	err := DB.Self.Where("uname = ?", name).First(&result).Error

	if err != nil {
		return ""
	}

	role := &Role{}
	log.Info("role === ", role.RoleID)
	rerr := DB.Self.Where("role_id = ?", result.RoleID).First(&role).Error
	if rerr != nil {
		return ""
	}

	return role.Rname
}
