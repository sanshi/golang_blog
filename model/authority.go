package model

import (
	"time"
)

// Role 用户角色
type Role struct {
	RoleID     uint64    `json:"roleId"`
	Rname      string    `json:"rname"`
	OptionUser string    `json:"optionUser"`
	CreatedAt  time.Time `json:"createdAt"`
	UpdatedAt  time.Time `json:"updatedAt"`
}

func (r *Role) TableName() string {
	return "blog_role"
}

// UserAuthority 角色权限
type UserAuthority struct {
	Uname      string    `json:"uname"`
	RoleID     uint64    `json:"roleId"`
	CreatedAt  time.Time `json:"createdAt"`
	OptionUser string    `json:"optionUser"`
}

func (ua *UserAuthority) TableName() string {
	return "user_authority"
}

// FindRole 设置角色
func CreateRole(uname string) (bool, string) {
	ua := &UserAuthority{
		Uname:      uname,
		RoleID:     2,
		CreatedAt:  time.Now(),
		OptionUser: "sanshi",
	}
	if err := DB.Self.Create(&ua).Error; err != nil {
		return false, "COMMON"
	}

	return true, "COMMON"
}
