package model

import (
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
)

const (
	// DEFAULTLIMIT 默认每页条数
	DEFAULTLIMIT = 10
	// DEFAULTPAGE 默认第几页开始
	DEFAULTPAGE = 0
)

// DB 数据库实例
var DB *Database

// Database 数据库
type Database struct {
	Self *gorm.DB
	// Docker *gorm.DB
}

// Init 初始化数据库
func (db *Database) Init() {
	DB = &Database{
		Self: GetSelfDB(),
		// Docker: GetDockerDB(),
	}
}

func openDB() *gorm.DB {
	var DBconfig = make([]string, 5)

	str := []string{"host", "user", "dbname", "sslmode", "password"}

	for _, s := range str {
		DBconfig = append(DBconfig, s+"="+viper.GetString("db."+s))
	}

	db, err := gorm.Open("postgres", strings.Join(DBconfig, " "))
	if err != nil {
		log.Fatal("Open postgres failure", err)
	}

	setupDB(db)

	return db
}

func setupDB(db *gorm.DB) {
	db.LogMode(true)
	// 用于设置最大打开链接数
	// db.DB().SetMaxOpenConns(20000)
	// 用于设置闲置的连接数
	db.DB().SetMaxIdleConns(0)
}

// GetSelfDB 获取数据库
func GetSelfDB() *gorm.DB {
	return openDB()
}

// Close 关闭数据库
func (db *Database) Close() {
	DB.Self.Close()
}

// func GetDockerDB() *gorm.DB {
// }
