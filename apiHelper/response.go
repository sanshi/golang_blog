package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// ResponseData 接口返回的结构体
type ResponseData struct {
	Data    interface{} `json:"data"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
}

// Response 通用的接口返回方法
func Response(c *gin.Context, code int, data interface{}) {
	c.JSON(http.StatusOK, ResponseData{
		Code:    code,
		Message: GetMsg(code),
		Data:    data,
	})
}
