package api

var Msg = map[int]string{
	SUCCESS:                "OK",
	INTERNAL_SERVER_ERROR:  "FAIL",
	INVALID_REQUEST_PARAMS: "请求参数错误",

	USER_NAME_EXISTED:     "用户名已经存在",
	USER_EMAIL_EXISTED:    "当前邮箱已经注册过",
	USER_LOGIN_INVALIDPWD: "密码格式不正确",
	USER_LOGIN_NILEMUN:    "请输入用户名或者邮箱",
	USER_CREATE_ERROR:     "创建用户出错",
	USER_NOT_FOUND:        "没有找到用户",
	USER_INVALID_PWD:      "密码不正确",
	USER_LOGIN_SUCCESS:    "用户登录成功",
	USER_NOT_LOGIN:        "用户没有登录",
	USER_NO_PRIVILEGE:     "当前用户没有权限",
	USER_CREATE_SUCCESS:   "用户注册成功",
	USER_HAD_PRIVILEGE:    "用户拥有权限",
	USER_LOGOUT_SUCCESS:   "退出登录成功",

	ARTICLE_CREATE_ERROR:   "文章创建出错",
	ARTICLE_CREATE_SUCCESS: "文章创建成功",

	SQL_HANDLE_ERR:         "查询出错",
	REQUEST_HEADER_MISSING: "请求头部解析出错",
	PARSE_TOKEN_FAILURE:    "请重新登录",
}

// GetMsg 同过code获取对应的message
func GetMsg(code int) string {
	msg, ok := Msg[code]
	if ok {
		return msg
	}

	return Msg[INTERNAL_SERVER_ERROR]
}
