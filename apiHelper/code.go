package api

const (
	SUCCESS                = 200
	INTERNAL_SERVER_ERROR  = 500
	INVALID_REQUEST_PARAMS = 400

	USER_NAME_EXISTED     = 200101
	USER_EMAIL_EXISTED    = 200102
	USER_LOGIN_INVALIDPWD = 200103
	USER_LOGIN_NILEMUN    = 200104
	USER_NOT_FOUND        = 200105
	USER_INVALID_PWD      = 200106
	USER_NOT_LOGIN        = 200107
	USER_LOGIN_SUCCESS    = 200108
	USER_CREATE_ERROR     = 200109
	USER_NO_PRIVILEGE     = 200110
	USER_CREATE_SUCCESS   = 200111
	USER_HAD_PRIVILEGE    = 200112
	USER_LOGOUT_SUCCESS   = 200113

	ARTICLE_CREATE_ERROR   = 200201
	ARTICLE_CREATE_SUCCESS = 200200

	SQL_HANDLE_ERR         = 100101
	REQUEST_HEADER_MISSING = 100102
	PARSE_TOKEN_FAILURE    = 100103
)

// 【1 服务器| 2普通错误】【  00 - 99 服务模块代码】 【00 具体错误代码】
