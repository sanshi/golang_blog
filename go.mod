module blog

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/jinzhu/gorm v1.9.4
	github.com/keegancsmith/rpc v1.1.0 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.2.0+incompatible
	github.com/lestrrat-go/strftime v0.0.0-20180821113735-8b31f9c59b0f // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/nsf/gocode v0.0.0-20190302080247-5bee97b48836 // indirect
	github.com/ramya-rao-a/go-outline v0.0.0-20181122025142-7182a932836a // indirect
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/sirupsen/logrus v1.4.1
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.3.2
	golang.org/x/crypto v0.0.0-20190426145343-a29dc8fdc734
	golang.org/x/tools v0.0.0-20190428024724-550556f78a90 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/go-playground/validator.v9 v9.28.0
)
