CREATE TABLE IF NOT EXISTS user_info (
  id SERIAL PRIMARY KEY,
  email varchar(128) NOT NULL,
  username varchar(31) NOT NULL,
  password  char(63) NOT NULL,
  passcode char(12) NOT NULL,
  login_ip varchar(31) NOT NULL,
  login_time timestamp(6) NOT NULL,
  created_at timestamp(6) NOT NULL,
  updated_at timestamp(6) NOT NULL,
  deleted_at timestamp(6)
);
ALTER TABLE user_info OWNER TO "sanshi";


CREATE TABLE IF NOT EXISTS articles (
  id SERIAL PRIMARY KEY,
  title varchar(127) NOT NULL,
  author varchar(63) NOT NULL,
  content varchar(1000) NOT NULL,
  abbr_content varchar(100) NOT NULL,
  markdown integer NOT NULL,
  pub_date timestamp(6) NOT NULL,
  created_at timestamp(6) NOT NULL,
  tags varchar(63) NOT NULL,
  views integer NOT NULL,
  comments integer NOT NULL,
  likes integer NOT NULL,
  tops integer NOT NULL
);
ALTER TABLE articles OWNER TO "sanshi";

CREATE TABLE IF NOT EXISTS  blog_role (
  role_id SERIAL PRIMARY KEY,
  rname varchar(63) NOT NULL,
  option_user varchar(31) NOT NULL,
  created_at timestamp(6) NOT NULL,
  updated_at timestamp(6) NOT NULL
);
ALTER TABLE blog_role OWNER TO "sanshi";

CREATE TABLE IF NOT EXISTS user_authority (
  id SERIAL PRIMARY KEY,
  uname varchar(31) NOT NULL,
  role_id int4 NOT NULL,
  created_at timestamp(6) NOT NULL,
  option_user varchar(31) NOT NULL
);
ALTER TABLE user_authority OWNER TO "sanshi";