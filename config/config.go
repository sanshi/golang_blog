package config

import (
	"strings"
	"time"

	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type Config struct {
	Name string
}

func Init(cfg string) error {
	c := Config{
		Name: cfg,
	}

	// 初始化配置
	if err := c.initConfig(); err != nil {
		return err
	}

	// 初始化日志
	c.initLog()

	return nil
}

func (c *Config) initConfig() (err error) {

	if c.Name != "" {
		viper.SetConfigFile(c.Name)
	} else {
		viper.AddConfigPath("config")
		viper.SetConfigName("config")
	}

	viper.SetConfigType("yaml")
	// 读取环境变量
	viper.AutomaticEnv()

	// 读取环境变量的前缀
	viper.SetEnvPrefix("BSERVER")

	//绑定环境
	viper.BindEnv("mode")
	viper.BindEnv("logpath")

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	return err
}

func (c *Config) initLog() {
	mode := viper.Get("mode")
	var currentMode string
	if mode == nil {
		currentMode = "debug"
	} else {
		currentMode = mode.(string)
	}

	var baseLogPath string
	if currentMode == "debug" {
		log.SetReportCaller(true)
		baseLogPath = viper.GetString("log.path")
	}
	if currentMode == "release" {
		logPath := viper.Get("logpath")
		baseLogPath = logPath.(string)
	}

	writer, err := rotatelogs.New(
		baseLogPath+".%Y%m%d%H%M",
		rotatelogs.WithLinkName(baseLogPath),      // 生成软链，指向最新日志文件
		rotatelogs.WithMaxAge(30*24*time.Hour),    // 文件最大保存时间
		rotatelogs.WithRotationTime(24*time.Hour), // 日志切割时间间隔
	)

	if err != nil {
		log.Error("config local file system logger error.")
	}

	log.AddHook(lfshook.NewHook(
		lfshook.WriterMap{
			log.InfoLevel:  writer,
			log.ErrorLevel: writer,
		},
		&log.JSONFormatter{
			TimestampFormat: "2006-01-02 15:04:05",
		},
	))
}
