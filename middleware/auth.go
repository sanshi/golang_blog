package middleware

import (
	api "blog/apiHelper"
	helper "blog/helper"

	"github.com/gin-gonic/gin"
)

//Middleware functions
func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if _, code := helper.ParseRequest(c); code != api.SUCCESS {
			api.Response(c, api.USER_NOT_LOGIN, nil)
			c.Abort()
			return
		}
		//Code for middleware
		c.Next()
	}
}
